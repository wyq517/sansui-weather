# sansui-Weather

#### 介绍
定制化天气预报消息推送（练手小脚本）

Python脚本实现天气查询应用，提醒她注意保暖！

#### 功能介绍

1. 天气信息获取
2. 当天天气信息提示
3. 第二天天气信息提示
4. 网易云热评信息获取
5. 疫情信息数据获取
6. 可根据个人需要，添加定制的信息提示

#### 使用说明

以下信息换为自己的，具体根据API地址申请key后进行替换

SendKey server酱

Weather_key 和风天气API

Tianapi_key 网易云热评API

Storeapi_key 疫情数据API

ApiKey 疫情数据API

userid 接收消息的用户，存在多个以 | 隔开

adminUserId 管理员用户，错误信息要推送的用户


#### 项目部署
[腾讯云函数部署教程](https://support.qq.com/products/130099/blog/546919)

### 使用教程
[使用教程](https://www.cnblogs.com/sansui6/p/15861636.html)




