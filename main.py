# -*- coding: utf8 -*-
import requests
import datetime
import time
import json
import pytz
import ssl,hashlib,string
from urllib import request,parse,error;
from urllib.parse import quote

SendKey = '';
Weather_key = ''
Tianapi_key = ''
Storeapi_key = ''
ApiKey = ''
# 接收消息的用户，存在多个以 | 隔开
userid = ''

adminUserId = '';

def get_time():
    nowTime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime());  # 获取当前时间
    # 由于腾讯云时间不准确，暂时使用年月日
    nowTime_1 = time.strftime("%Y-%m-%d", time.localtime());  # 获取当前时间
    # print(nowTime_1)
    return nowTime_1;


#获取网易云评论
def get_wyy():
    list = []  ## 空列表
    wyy_url = "http://api.tianapi.com/txapi/hotreview/index?key=" + Tianapi_key + "";
    wyy_json = requests.post(wyy_url).json();
    wyy_code = wyy_json["code"]
    if wyy_code.__eq__(200):
        #获取评论内容
        wyy_content = wyy_json["newslist"][0]["content"]
        #获取歌名
        wyy_source = wyy_json["newslist"][0]["source"]
        #把评论、歌名放进数组
        list.append(wyy_content)
        list.append(wyy_source)
        return list;
    else:
        list.append('获取失败！')
        return list;

#2021/8/4 因疫情较为严重，新增加疫情提示
def ncov():
    api_url = 'https://yupn.api.storeapi.net/api/94/221';
    appid = appid需要填写;
    secret = Storeapi_key;
    data = {
        'appid': appid,
        'city_name': '郑州',
        'format': 'json',
        'time': '1545829466',
    };
    data['appid'] = appid;
    data['time'] = round(time.time());  # 当前服务器时间
    keysArr = list(data.keys())  # 取出字典key
    keysArr.sort()  # 对字典key进行排序
    md5String = '';
    params = []
    for key in keysArr:
        if data[key]:
            val = str(data[key])
            md5String += key + val
            params.append(key + "=" + val)
    md5String += secret;
    m = hashlib.md5()
    b = md5String.encode(encoding='utf-8')
    m.update(b)
    sign = m.hexdigest()

    params.append('sign=' + sign)  # 加入计算后的sign值去请求
    params = '&'.join(tuple(params));  # 把列表转成元组后用&分隔，最终转换成字符串 a=b&c=d&e=f

    ssl._create_default_https_context = ssl._create_unverified_context
    url = api_url + '?' + params;
    url = quote(url, safe=string.printable)
    req = requests.get(url).json()
    #city 所选城市
    #confirm 确诊病例数量
    #curConfirm 当前确诊数量
    #died  死亡数量
    #heal  治愈数量
    #asymptomatic 无症状感染数量
    #print(req["retdata"])
    return req["retdata"]

#获取实时疫情资讯，如分风险地区、疫情概况、疫情新闻等
def nCoV_news(num):
    url="http://api.tianapi.com/txapi/ncov/index?key="+ApiKey;
    nCoV_news_json = requests.get(url).json()
    nCoV_news_code = nCoV_news_json["code"]
    nCoV_news_newslist = nCoV_news_json["newslist"]
    i=0;
    if(nCoV_news_code.__eq__(200)):
        if (num.__eq__(1)):
            # 获取当前数据信息
            return nCoV_news_newslist[0]["desc"]
        elif(num.__eq__(2)):
            #返回全国风险地区，high高风险、mid中风险
            riskarea_high_text = "| -------- | 高风险地区 | ----------------: |";
            for riskarea_high in nCoV_news_newslist[0]["riskarea"]["high"]:
                if("河南省" in riskarea_high):
                    # 因为高风险地区逐渐增多，消息已经存放不下，现调整通知格式为具体数量，不再是具体地区。
                    i=i+1;
            return "\n\n河南省高风险地区共：" + str(i) + "个。\n\n"
        elif(num.__eq__(0)):
            riskarea_mid_text = "| -------- | 中风险地区 | ----------------: |";
            for riskarea_mid in nCoV_news_newslist[0]["riskarea"]["mid"]:
                # print(riskarea_high)
                if ("河南省" in riskarea_mid):
                    #因为中风险地区逐渐增多，消息已经存放不下，现调整通知格式为具体数量，不再是具体地区。
                    #riskarea_mid_text +="\n\n" + riskarea_mid;
                    # riskarea_high_text=riskarea_high;
                    i=i+1;
            # return nCoV_news_newslist[0]["riskarea"]
            #print(riskarea_mid_text)
            #return riskarea_mid_text;
            return "\n\n河南省中风险地区共："+str(i)+"个。\n\n更多疫情相关内容[点击此处](https://ncov.dxy.cn/ncovh5/view/pneumonia)查看。"


#获取当天天气，不过没有最高温度，最低温度
def get_Weather():
    city_id = get_location();
    Weather_url = f'https://devapi.qweather.com/v7/weather/now?key={Weather_key}&location={city_id}'
    Weather_json = requests.get(Weather_url).json()
    #print(Weather_url)
    Weather_code = Weather_json["code"]
    Weather_now = Weather_json["now"]
    if(Weather_code.__eq__("200")):
      #print(Weather_now)
      return Weather_now;

#获取包括今天在内的未来三天天气
def get_3d_Weather(num):
    date = get_time();
    date_tomorrow = (datetime.datetime.now()+datetime.timedelta(days=1)).strftime("%Y-%m-%d");
    city_id = get_location();
    Weather_url = f'https://devapi.qweather.com/v7/weather/3d?key={Weather_key}&location={city_id}';
    #print(Weather_url)
    Weather_json = requests.get(Weather_url).json();
    Weather_3d = Weather_json["daily"]
    for weather in Weather_3d:
        #print("6666666"+weather)
        if(num.__eq__(1)):
            if(weather["fxDate"].__eq__(date)):
                #print("当天天气获取成功")
                return weather;
            else:
                print("查询的不是今天日期的天气")
                return "查询的不是今天日期的天气"
                break;
        elif(num.__eq__(0)):
            if (weather["fxDate"].__eq__(date_tomorrow)):
                #print(weather);
                return weather;
        else:
            print("非法参数！")
            return "非法参数！"

#获取当前天气地区id
def get_location():
    # 调整所在的区域
    location='郑州'
    Weather_city_url = f'https://geoapi.qweather.com/v2/city/lookup?key={Weather_key}&location={location}'
    city_json =requests.get(Weather_city_url).json();
    city_code = city_json["code"]
    if city_code.__eq__("200"):
     #print(Weather_city_url)
     for Weather_city in city_json["location"]:
         Weather_city_name = Weather_city["name"]
         if Weather_city_name.__eq__("二七"):
             #print(Weather_city)
             city_id = Weather_city["id"]
             #print(city_id)
             return city_id;

    else:
      print("访问获取地区接口失败！")
      return "访问获取地区接口失败！";

#拼接消息
def get_desp():

    wyy_content = get_wyy()[0];  # 评论内容
    wyy_source = get_wyy()[1];  # 歌名
    date_tomorrow = (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%Y-%m-%d");

    Weather = get_Weather() #获取当天天气信息
    Weather_text = Weather["text"] #获取当天天气文字
    Weather_temp = Weather["temp"] #实时获取当前温度
    Weather_feelsLike = Weather["feelsLike"] #获取当前体感温度

    Weather_3d_day = get_3d_Weather(1)  # 获取当天的预报天气
    Weather_3d_day_tempMax = Weather_3d_day["tempMax"] #获取当天预报最高温度
    Weather_3d_day_tempMin = Weather_3d_day["tempMin"] #获取当天预报最低温度
    Weather_3d_day_uvIndex = Weather_3d_day["uvIndex"] #获取当天紫外线强度

    Weather_3d_tomorrow = get_3d_Weather(0)  # 获取明天天气
    Weather_3d_tomorrow_textDay = Weather_3d_day["textDay"]  # 获取第二天天气文字
    Weather_3d_tomorrow_tempMax = Weather_3d_day["tempMax"]  # 获取第二天预报最高温度
    Weather_3d_tomorrow_tempMin = Weather_3d_day["tempMin"]  # 获取第二天预报最低温度
    Weather_3d_tomorrow_uvIndex = Weather_3d_day["uvIndex"]  # 获取第二天紫外线强度

    cur_time = time.time()
    #print(cur_time)
    local_tz = pytz.timezone("Asia/Shanghai")
    local_time = datetime.datetime.fromtimestamp(cur_time, tz=local_tz).hour
    # print(local_time)

    Ncov_zz = ncov();
    # city 所选城市
    Ncov_city = Ncov_zz["city"];
    # confirm 确诊病例数量
    Ncov_confirm = Ncov_zz["confirm"];
    # curConfirm 当前确诊数量
    Ncov_curConfirm = Ncov_zz["curConfirm"];
    # died  死亡数量
    # heal  治愈数量
    Ncov_heal = Ncov_zz["heal"];
    # asymptomatic 无症状感染数量

    nCoV_news_num = nCoV_news(1);#获取全国疫情数据
    nCoV_news_high = nCoV_news(2);  # 获取疫情高风险地区
    nCoV_news_mid = nCoV_news(0);  # 获取疫情中风险地区
    currentConfirmedCount  = nCoV_news_num["currentConfirmedCount"]#现存确诊人数
    seriousCount = nCoV_news_num["seriousCount"] #现存无症状
    beijing_Time = datetime.datetime.fromtimestamp(cur_time, tz=local_tz).strftime("%Y-%m-%d %H:%M:%S");

    Ncov_desp = ("\n\n| -------- |全国疫情实时播报|----------------: |\n\n"+
    "截至北京时间 "+beijing_Time+"\n\n"+
    "全国现存确诊病例:" + str(currentConfirmedCount)+"\n\n"+
    "全国现存无症状病例:" + str(seriousCount) + "\n\n" +
    Ncov_city +"现存确诊病例:" + str(Ncov_curConfirm) + "\n\n" +
     nCoV_news_high + "\n\n" + nCoV_news_mid)


    desp_Weather_temp_text = "";
    desp_Weather_text = "";
    desp_Weather_3d_tomorrow_textDay_text = "";
    desp_Weather_3d_day_tempMax_text = "";
    desp_Weather_3d_tomorrow_tempMax_text = "";
    desp_day = "";
    desp_tomorrow = "";
    desp_Weather_3d_day_uvIndex = "";

    # 晚上八点 查询第二天天气情况,然后根据当前时间，定制化推送当前提醒
    if (local_time >= 20 and local_time <= 23):
        # 如果有雨，不校验最高温度
        # 判断天气
        if ("雨" in Weather_3d_tomorrow_textDay):
            if Weather_3d_tomorrow_textDay.__eq__("大雨"):
                desp_Weather_3d_tomorrow_tempMax_text = "\n\n";
                desp_Weather_3d_tomorrow_textDay_text = "\n\n明天"+ Weather_3d_tomorrow_textDay + ",傻瓜，记得带伞，记得早点出门，晚上早点睡\n\n"
            elif Weather_3d_tomorrow_textDay.__eq__("中雨"):
                desp_Weather_3d_tomorrow_tempMax_text = "\n\n";
                desp_Weather_3d_tomorrow_textDay_text = "\n\n明天" + Weather_3d_tomorrow_textDay + ",傻瓜，记得带伞，记得早点出门，晚上早点睡\n\n"
            elif Weather_3d_tomorrow_textDay.__eq__("雷阵雨"):
                desp_Weather_3d_tomorrow_tempMax_text = "\n\n";
                desp_Weather_3d_tomorrow_textDay_text = "\n\n明天" + Weather_3d_tomorrow_textDay + ",小心打雷，抱抱傻瓜，不怕不怕\n\n"
            else:
                desp_Weather_3d_tomorrow_tempMax_text = "\n\n";
                desp_Weather_3d_tomorrow_textDay_text = "\n\n明天" + Weather_3d_tomorrow_textDay + ",记得带伞，请注意天气\n\n"
        # 没雨的情况下，根据温度提醒明天天气
        else:
            desp_Weather_3d_tomorrow_textDay_text = "\n\n";
            if (int(Weather_3d_tomorrow_tempMax) >= 33):
                desp_Weather_3d_tomorrow_tempMax_text = "\n\n明天天气较热，注意防晒！\n\n"
            else:
                desp_Weather_3d_tomorrow_tempMax_text = "\n\n";

        desp_tomorrow = (
                    "明天 " + date_tomorrow + "\n\n天气：" + Weather_3d_tomorrow_textDay + desp_Weather_3d_tomorrow_textDay_text +
                    "明天最高温度：" + Weather_3d_tomorrow_tempMax + "℃。" + desp_Weather_3d_tomorrow_tempMax_text +
                    "明天最低温度：" + Weather_3d_tomorrow_tempMin + "℃。"
                    );
    elif (local_time >= 0 and local_time < 20):
        # desp_Weather_3d_tomorrow_textDay_text = "\n\n";
        # desp_Weather_3d_tomorrow_tempMax_text = "\n\n";
        # 判断当前温度
        if (int(Weather_temp) >= 33):
            if (8 < local_time < 16):
                desp_Weather_temp_text = "，天气有点热喔！\n\n"
            else:
                desp_Weather_temp_text = "。\n\n";
        else:
            desp_Weather_temp_text = "。\n\n";

        # 判断紫外线等级
        if (8 <= local_time < 16):
            if (4 < int(Weather_3d_day_uvIndex) <= 6):
                desp_Weather_3d_day_uvIndex = "\n\n今天紫外线等级" + Weather_3d_day_uvIndex + ",适合户外运动。\n\n"
            elif (6 < int(Weather_3d_day_uvIndex) <= 9):
                desp_Weather_3d_day_uvIndex = "\n\n今天紫外线等级" + Weather_3d_day_uvIndex + ",请注意防晒。\n\n"
            elif (9 < int(Weather_3d_day_uvIndex)):
                desp_Weather_3d_day_uvIndex = "\n\n今天紫外线等级" + Weather_3d_day_uvIndex + ",宝,外边太晒了,能不出去就不要出去了。\n\n"
            else:
                desp_Weather_3d_day_uvIndex = "\n\n";
        else:
            desp_Weather_3d_day_uvIndex = "\n\n";

            # 判断当天是否有雨，有雨不判断当天最高温度
            Weather_text = "晴"
        if ("雨" in Weather_text):
            if Weather_text.__eq__("大雨"):
                desp_Weather_3d_day_tempMax_text = "\n\n";
                desp_Weather_text = "\n\n当前有" + Weather_text + ",傻瓜，赶紧避雨，不要再外出啦\n\n"
            elif Weather_text.__eq__("中雨"):
                desp_Weather_3d_day_tempMax_text = "\n\n"
                desp_Weather_text = "\n\n当前有" + Weather_text + ",傻瓜，赶紧避雨，不要再外出啦\n\n"
            elif Weather_text.__eq__("雷阵雨"):
                desp_Weather_3d_day_tempMax_text = "\n\n"
                desp_Weather_text = "\n\n当前有" + Weather_text + ",小心打雷，抱抱傻瓜，不怕不怕\n\n"
            else:
                desp_Weather_3d_day_tempMax_text = "\n\n"
                desp_Weather_text = "\n\n当前有" + Weather_text + ",请注意天气\n\n"
        else:
            desp_Weather_text = "\n\n";
            # 判断当天最高温度
            if (int(Weather_3d_day_tempMax) >= 33):
                # 下午16点以后不用提示
                if (local_time > 16):
                    desp_Weather_3d_day_tempMax_text = "\n\n";
                else:
                    desp_Weather_3d_day_tempMax_text = "\n\n今天天气较热，注意防晒！\n\n"
            else:
                desp_Weather_3d_day_tempMax_text = "\n\n";

        desp_day = (
                "当前天气：" + Weather_text + desp_Weather_text +
                "当前温度：" + Weather_temp + "℃" + desp_Weather_temp_text +
                "今天最高温度：" + Weather_3d_day_tempMax + "℃。" + desp_Weather_3d_day_tempMax_text +
                "最低温度：" + Weather_3d_day_tempMin + "℃。" + desp_Weather_3d_day_uvIndex  # 紫外线 16点后不提示
            # "风力等级：" + Weather["windScale"] + "\n\n\n"
        );
    ##以下是通知内容！
    desp = "郑州市二七区\n\n " + desp_day + desp_tomorrow + Ncov_desp;
    return desp;


# 程序入口，消息推送
def main():
    title = '叮咚-查收天气情况';
    desp = get_desp();
    url = f"https://sctapi.ftqq.com/{SendKey}.send?title={title}&desp={desp}&openid={userid}";

    res = requests.post(url);
    if res.status_code == 200:
        print("成功！")
    else:
       title = "天气推送失败！"
       ##desp = "尊敬的管理员，消息推送失败\n\n推送日志"+res.json();
       print(res.status_code)
       #url = f"https://sctapi.ftqq.com/{SendKey}.send?title={title}&desp={desp}&openid={adminUserId}";
       #res = requests.post(url);
       print("错误日志推送成功 ！")


if __name__ == '__main__':
   main();
   #nCoV_news(0)

#def main_handler(event, context):
 #  main()
